
const assert = require('assert');
const myFunction = require('./homework5-1.js')
const genderData = myFunction.genderData
const friendData = myFunction.friendData
const eyeData = myFunction.eyeData

const fs = require('fs');

describe('tdd lab', function () {
    describe('#checkFileExist()', function () {
        //ตรวจเช็คไฟล์ว่ามีหรือไม่
        // it('should have homework5-1_eyes.json existed', function () {
        //     assert.equal(fs.existsSync('homework5-1_eyes.json'), true, 'File not found.');
        // });
        it('should have homework5-1_gender.json existed', function () {
            assert.equal(fs.existsSync('homework5-1_gender.json'), true, 'File not found.');
        });
        // it('should have homework5-1_friends.json existed', function () {
        //     assert.equal(fs.existsSync('homework5-1_friends.json'), true, 'File not found.');
        // });
    });



    describe('#objectKey()', function () {
        //ตรวจเช็คคีย์ว่าตรงตามที่ต้องการหรือป่าว
        it('should have same object key stucture as homework5-1_eyes.json', async function () {
            let eyeKey = await eyeData('/homework1-4.json')
            let checkEyeKey = { "brown": 4,"green": 3, "blue": 5};
            assert.deepEqual(Object.keys(eyeKey), Object.keys(checkEyeKey), 'Read JSON data is not equal objectkeys.');
        });
        it('should have same object key stucture as homework5-1_gender.json', async function () {
            let friendKey =  await genderData('/homework1-4.json');
            let checkFriendKey = {"male": 0,"female": 0};
          
                assert.deepEqual(Object.keys(friendKey), Object.keys(checkFriendKey), 'Read JSON data is not equal objectkeys.');
 
        });
        it('should have same object key stucture as homework5-1_friends.json', async function () {
            let genderJSON = await friendData('/homework1-4.json')
            let genderKey;
            genderJSON.forEach(genderObj => {
                genderKey = genderObj;
            });
            let checkGenderKey = {"_id": "0","friendsCount": 0};
            assert.deepEqual(Object.keys(genderKey), Object.keys(checkGenderKey), 'Read JSON data is not equal objectkeys.');
        });
    });



    // describe('#userFriendCount()', function() {
    //     it('should have size of array input as 23', function() {

    //     });
    // });
    // describe('#sumOfEyes()', function() {
    //     it('should have sum of eyes as 23', function() {

    //     });
    // });

    // describe('#sumOfGender()', function() {
    //     it('should have sum of gender as 23', function() {

    //     });
    // });
    
});