const fs = require('fs')

//------------ เขียนไฟล์ --------------------------------- //

function myWriteFile(fileName,data){
    return new Promise((resolve,reject)=>{
        data = JSON.stringify(data)
    fs.writeFile(fileName,data , 'utf8', function (err) {
        if (err){ 
            reject(error)
        }else {
        console.log(`WriteFile DATA in ${fileName} Success`)
            resolve(true)
        }
        
    })
})
}


// --------------- อ่านไฟล์ ---------------------------//
function myReadFile(fileName){
    return new Promise((resolve,reject)=>{
    fs.readFile(__dirname+fileName,'utf8', (error, dataJSON) => {
        if(error){
            reject(error)
        }else{
        //    dataJSON = JSON.parse(dataJSON)
            //console.log(dataJSON)
            resolve(dataJSON)
        }
    })
})
}







//------------------- เช็คจำนวนสีตา สีละกี่คน -----------------------// 

async function  eyeData(fileName){
    try{
        let dataRead = await myReadFile(fileName)
        let eye = await eyeColorSum(dataRead)
        await myWriteFile('homework5-1_eyes.json',eye)
        return eye
    //    console.log(eye)
    }catch (error){
        console.error(error)
    }
//--------------------------------------------------------------//
function eyeColorSum(dataJSON) {
    dataJSON = JSON.parse(dataJSON)
            let eye = {}
            let count = 1
            dataJSON.forEach(function (dataObj) {
                let eyecol =  dataObj.eyeColor
                if(Object.keys(eye) !== eyecol){
                    eye[eyecol] = count
                }else{
                    eye[eyecol] += 1 
                }
            }) 
            return eye
}
//------------------------------------------------------------//
}






//---------------- เช็คจำนวนเพื่อน ----------------------------// 
async function  friendData(fileName){
    try{
        let dataRead = await myReadFile(fileName)
        let friend = await friendSum(dataRead)
        await myWriteFile('homework5-1_friends.json',friend)
        return friend
    //    console.log(friend)
    }catch (error){
        console.error(error)
    }
//---------------------------------------------------------//    
    function friendSum(dataJSON){
         dataJSON = JSON.parse(dataJSON)
         let newArray = []
         for(let peopleObj in dataJSON){
             let newObj = {}
            newObj._id = dataJSON[peopleObj]._id
            newObj.friendsCount = dataJSON[peopleObj].friends.length
            newArray[peopleObj] = newObj
         }
         return newArray
     }
//------------------------------------------------------------//
}





//---------------- เช็คจำนวนเพศ เพศละกี่คน--------------------// 
async function genderData(fileName){
    try{
        let dataRead = await myReadFile(fileName)
        let gender = await genderSum(dataRead)
        await myWriteFile('homework5-1_gender.json',gender)
        return gender
    }catch (error){
        console.error(error)
    }

//----------------------------------------------------------//

function genderSum(dataJSON){
    dataJSON = JSON.parse(dataJSON)
    let countGender = {"male":0,"female":0}
    for(let keygender in dataJSON){
        let sex = dataJSON[keygender].gender
        if(sex == "male"){ countGender.male++}
        else if (sex == "female"){countGender.female++}
    }
    return countGender
  //  })
}
//------------------------------------------------------------//
}



let a = '/homework1-4.json'
// genderData(a)
// friendData(a)
// eyeData(a)

exports.genderData = genderData
exports.friendData = friendData
exports.eyeData = eyeData
